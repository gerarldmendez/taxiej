@extends('layouts.pages')

@section('content')

@include('common.navbar_main')

<header class="page-header">
    <div class="container">
        <ul class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
        </ul>
        <h1>Quienes Somos</h1>
    </div>
</header>

<div class="container">
    <!-- Content -->
    <div class="margin-disabled">
        <div class="row">
            <div class=" col-md-12 text-page">
                <article id="post-701" class="post-701 page type-page status-publish hentry">
                    <div class="entry-content clearfix">
                        <div class="vc_row wpb_row vc_row-fluid vc_column-gap-15 vc_row-o-equal-height vc_row-flex">
                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class="vc_empty_space" style="height: 64px"><span class="vc_empty_space_inner"></span></div>

                                        <div class="wpb_text_column wpb_content_element ">
                                            <div class="wpb_wrapper">
                                                <p style="text-align: center;">MÁS DE 50 AÑOS DE EXPERIENCIA </p>
                                                <P style="text-aling"> Taxi Antequera es la asociación con la mayor flota de taxis en Oaxaca.
                                                  Csdvssd vdbd sfaad cdvdvdrgsd asdasf sg h fgfdghd
                                            </div>
                                        </div>
                                        <div class="vc_row wpb_row vc_inner vc_row-fluid vc_column-gap-30 vc_row-o-equal-height vc_row-flex">
                                            <div class="wpb_column vc_column_container vc_col-sm-4 vc_col-has-fill bg-color-gray">
                                                <div class="vc_column-inner vc_custom_1500835217408">
                                                    <div class="wpb_wrapper">
                                                        <div class="vc_icon_element vc_icon_element-outer vc_custom_1500833904120 vc_icon_element-align-center">
                                                            <div class="vc_icon_element-inner vc_icon_element-color-custom vc_icon_element-size-lg vc_icon_element-style- vc_icon_element-background-color-grey">
                                                                <span class="vc_icon_element-icon fa fa-location-arrow"
                                                                    style="color:#ffc61a !important"></span></div>
                                                        </div>

                                                        <div class="wpb_text_column wpb_content_element ">
                                                            <div class="wpb_wrapper">
                                                                <p style="text-align: center;"><strong>153 Puebla,<br>
                                                                        Puebla, C.P 7000</strong></p>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="wpb_column vc_column_container vc_col-sm-4 vc_col-has-fill bg-color-gray">
                                                <div class="vc_column-inner vc_custom_1500835222180">
                                                    <div class="wpb_wrapper">
                                                        <div class="vc_icon_element vc_icon_element-outer vc_custom_1500834576675 vc_icon_element-align-center">
                                                            <div class="vc_icon_element-inner vc_icon_element-color-custom vc_icon_element-size-lg vc_icon_element-style- vc_icon_element-background-color-grey">
                                                                <span class="vc_icon_element-icon fa fa-phone-square"
                                                                    style="color:#ffc61a !important"></span></div>
                                                        </div>

                                                        <div class="wpb_text_column wpb_content_element ">
                                                            <div class="wpb_wrapper">
                                                                <p style="text-align: center;"><strong>9512346910</strong></p>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="wpb_column vc_column_container vc_col-sm-4 vc_col-has-fill bg-color-gray">
                                                <div class="vc_column-inner vc_custom_1500835226344">
                                                    <div class="wpb_wrapper">
                                                        <div class="vc_icon_element vc_icon_element-outer vc_custom_1500834582341 vc_icon_element-align-center">
                                                            <div class="vc_icon_element-inner vc_icon_element-color-custom vc_icon_element-size-lg vc_icon_element-style- vc_icon_element-background-color-grey">
                                                                <span class="vc_icon_element-icon fa fa-envelope" style="color:#ffc61a !important"></span></div>
                                                        </div>

                                                        <div class="wpb_text_column wpb_content_element ">
                                                            <div class="wpb_wrapper">
                                                                <p style="text-align: center;"><strong><a style="color: #000000;"
                                                                            href="mailto:support@this-theme.com">soporte@widutek.com</a></strong></p>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="vc_empty_space" style="height: 20px"><span class="vc_empty_space_inner"></span></div>
                                        <div class="vc_row wpb_row vc_inner vc_row-fluid">
                                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_text_column wpb_content_element  vc_custom_1500760931508">
                                                            <div class="wpb_wrapper">
                                                                <p style="text-align: center;"><strong>Redes Sociales:</strong></p>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="align-center">
                                            <ul class="social-big">
                                                <li><a href="#" class="fa fa-facebook"></a></li>
                                                <li><a href="#" class="fa fa-twitter"></a></li>
                                                <li><a href="#" class="fa fa-youtube"></a></li>
                                                <li><a href="#" class="fa fa-instagram"></a></li>
                                            </ul>
                                        </div>
                                        <div class="vc_empty_space" style="height: 10px"><span class="vc_empty_space_inner"></span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                      
                                <!-- aqui va mapa de localizacion de la direccion de taxis-->
                                
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="vc_row-full-width vc_clearfix"></div>
                    </div>
                </article>
            </div>

        </div>
    </div>

</div>


@include('common.section_clients')

@endsection