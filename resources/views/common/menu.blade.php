
<!--/* menu horizontal home*/-->
<ul id="menu-main-menu" class="nav navbar-nav">
    <li id="menu-item-42" class="menu-item current-menu-parent">
        <a href="{{ url('/') }}/"><span>Home</span></a>
    </li>
    <li id="menu-item-50" class="menu-item">
        <a href="{{ url('/') }}/get-taxi/"><span>Reserva Taxi</span></a>
    </li>
    <li id="menu-item-55" class="menu-item">
        <a href="{{ url('/') }}/services/"><span>Servicios</span></a>
    </li>
    <li id="menu-item-59" class="menu-item">
        <a href="{{ url('/') }}/tarrif/"><span>Tarifas</span></a>
    </li>
    <li id="menu-item-61" class="menu-item">
        <a href="{{ url('/') }}/team/"><span>Conductores</span></a>
    </li>
    <li id="menu-item-61" class="menu-item">
        <a href="{{url('/')}}/earn-with-us"><span>Olvido algo?</span></a>
    </li>
    <li id="menu-item-707" class="menu-item">
        <a href="{{url('/')}}/contact-us"><span>Contacto</span></a>
    </li>
    <li id="menu-item-61" class="menu-item">
        <a href="{{url('/')}}/about-us"><span>Nosotros</span></a>
    </li>
</ul>