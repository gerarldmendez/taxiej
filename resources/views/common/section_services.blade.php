<section class="vc_section services-block">
    <div class="vc_row wpb_row vc_row-fluid">
        <div class="wpb_column vc_column_container vc_col-sm-12">
            <div class="vc_column-inner ">
                <div class="wpb_wrapper">
                    <div class="heading  heading-large align-center" id="like_sc_header_915230290">
                        <h4>Bienvenido</h4>
                        <h2>Nuestros Servicios</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="vc_row wpb_row vc_row-fluid vc_row-o-equal-height vc_row-flex">
        <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-3 vc_col-md-6">
            <div class="vc_column-inner ">
                <div class="wpb_wrapper">
                    <div class="wpb_single_image wpb_content_element vc_align_center">

                        <figure class="wpb_wrapper vc_figure">
                            <div class="vc_single_image-wrapper   vc_box_border_grey"><img width="71" height="71" src="/images/services-1.png"
                                    class="vc_single_image-img attachment-thumbnail" alt=""></div>
                        </figure>
                    </div>

                    <div class="wpb_text_column wpb_content_element ">
                        <div class="wpb_wrapper">
                            <h5>Traslados dentro y fuera de la ciudad</h5>
                            <p>Lo llevaremos rápida y cómodamente a cualquier lugar de
                                la ciudad</p>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-3 vc_col-md-6">
            <div class="vc_column-inner ">
                <div class="wpb_wrapper">
                    <div class="wpb_single_image wpb_content_element vc_align_center">

                        <figure class="wpb_wrapper vc_figure">
                            <div class="vc_single_image-wrapper   vc_box_border_grey"><img width="71" height="71" src="/images/services-2.png"
                                    class="vc_single_image-img attachment-thumbnail" alt=""></div>
                        </figure>
                    </div>

                    <div class="wpb_text_column wpb_content_element ">
                        <div class="wpb_wrapper">
                            <h5>RESERVAR UN HOTEL</h5>
                            <p>Si necesita un hotel cómodo, nuestros operadores lo reservarán para usted y tomarán un taxi a la dirección</p>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-3 vc_col-md-6">
            <div class="vc_column-inner ">
                <div class="wpb_wrapper">
                    <div class="wpb_single_image wpb_content_element vc_align_center">

                        <figure class="wpb_wrapper vc_figure">
                            <div class="vc_single_image-wrapper   vc_box_border_grey"><img width="68" height="68" src="/images/services-3.png"
                                    class="vc_single_image-img attachment-thumbnail" alt=""></div>
                        </figure>
                    </div>

                    <div class="wpb_text_column wpb_content_element ">
                        <div class="wpb_wrapper">
                            <h5>Traslados desde el aeropuerto</h5>
                            <p>Te llevamos y recogemos del aeropuerto.</p>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-3 vc_col-md-6">
            <div class="vc_column-inner ">
                <div class="wpb_wrapper">
                    <div class="wpb_single_image wpb_content_element vc_align_center">

                        <figure class="wpb_wrapper vc_figure">
                            <div class="vc_single_image-wrapper   vc_box_border_grey"><img width="72" height="60" src="/images/services-4.png"
                                    class="vc_single_image-img attachment-thumbnail" alt=""></div>
                        </figure>
                    </div>

                    <div class="wpb_text_column wpb_content_element ">
                        <div class="wpb_wrapper">
                            <h5>TRANSPORTE DE EQUIPAJE</h5>
                            <p>Trasportamos tu equipaje a la direccion que nos indiques</p>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>