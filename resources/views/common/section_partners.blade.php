<section data-vc-full-width="true" data-vc-full-width-init="true" class="vc_section" style="position: relative; left: -43px; box-sizing: border-box; width: 1286px; padding-left: 43px; padding-right: 43px;">
    <div data-vc-full-width="true" data-vc-full-width-init="true" data-vc-stretch-content="true" class="vc_row wpb_row vc_row-fluid vc_row-no-padding car-block"
        style="position: relative; left: -43px; box-sizing: border-box; width: 1286px;">
        <div class="wpb_column vc_column_container vc_col-sm-12">
            <div class="vc_column-inner ">

                <div class="wpb_wrapper">
                    <div class="car-right animation-block">
                        <img src="/images/car-big-side.png" class="full-width slideleft" alt="animation">
                    </div>
                </div>
            </div>
        </div>
    </div>
                <div class="wpb_wrapper">
                    <div class="vc_row wpb_row vc_inner vc_row-fluid">
                        <!--esàcop entre el menu y el formulario-->
                        <div class="wpb_column vc_column_container vc_col-sm-12 vc_hidden-sm vc_hidden-xs">
                            <div class="vc_column-inner ">
                                <div class="wpb_wrapper">
                                    <div class="vc_empty_space" style="height: 75px"><span class="vc_empty_space_inner"></span></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="heading  heading-large align-left" id="like_sc_header_158449325">
                        <h4>Recupera lo que olvidaste</h4>
                        <h2>Que olvidaste en el taxi?</h2>
                    </div>

                    <div class="vc_row wpb_row vc_inner vc_row-fluid">
                        <div class="wpb_column vc_column_container vc_col-sm-6">
                                    <div class="wpb_text_column wpb_content_element ">
                                        <div class="wpb_wrapper">
                                            <p>En el caso de que hayas perdido algún objeto en algún taxi de Antequera contacta con nosotros por teléfono o rellenar el formulario que aparece a continuación para que nuestro personal lo revise. Recuerda que cuanta más información recibamos más fácil será comprobar si el objeto perdido se encuentra en algún vehículo de nuestra flota.</p>

                                        </div>
                                    </div>

                                    <div role="form" class="wpcf7" id="wpcf7-f200-p25-o1" lang="en-US" dir="ltr">
                                            
                                            <form action="/contacts/#wpcf7-f200-p25-o1" method="post" class="wpcf7-form"
                                                novalidate="novalidate">
                                                <p><label> Nombre*<br>
                                                        <span class="wpcf7-form-control-wrap your-name"><input type="text"
                                                                name="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                                                aria-required="true" aria-invalid="false"></span>
                                                    </label></p>
                                                <p><label> Email *<br>
                                                        <span class="wpcf7-form-control-wrap your-email"><input type="email"
                                                                name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email"
                                                                aria-required="true" aria-invalid="false"></span>
                                                    </label></p>
                                                <p><label> Descripcion del objeto*<br>
                                                        <span class="wpcf7-form-control-wrap your-message"><textarea
                                                                name="your-message" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea"
                                                                aria-invalid="false"></textarea></span> </label></p>
                                                <p><input type="submit" value="Enviar" class="wpcf7-form-control wpcf7-submit"><span
                                                        class="ajax-loader"></span></p>
                                            </form>
                                        </div> 
                            </div>
                        </div>
                        
                    </div>
</section>